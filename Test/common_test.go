package tests

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/champinfo/go/cachemanager"
)

const (
	HashKey = "product"
	SubKey1 = "1"
	SubKey2 = "2"
	SubKey3 = "3"
	//Value1            = "value1"
	//Value2            = "value2"
	//Value3            = "value3"
	ExpireSecondsKey  = "expire-seconds-key"
	ExpireDaysKey     = "expire-days-key"
	SetExSecondsKey   = "set-expire-seconds-key"
	IncreaseByKey     = "increase-by-key"
	DecreaseByKey     = "decrease-by-key"
	HashIncreaseByKey = "hash-increase-by-key"
	IncreaseByValue   = 1
	DecreaseByValue   = 3
)

const (
	Value1 = "{\n    \"data\": {\n        \"id\": 1,\n        \"full_name\": \"京都交通票-1日票\",\n        \"short_name\": \"KyotoTransport-OneDayPass\",\n        \"title\": \"京都交通票-1日票\",\n        \"sub_title\": \"1日票\",\n        \"code\": \"9c1e25ed-26e4-4210-95ee-98be0137ed6f\",\n        \"notice\": \"demo trans for Kansai\",\n        \"release_date\": \"2022-05-23T19:24:28Z\",\n        \"validate_date_s\": \"2022-05-23T19:24:28Z\",\n        \"validate_date_e\": \"2022-05-31T23:59:59Z\",\n        \"status\": 1,\n        \"type\": 2,\n        \"quantity\": -1,\n        \"publisher_id\": 1,\n        \"auto_fetch\": 1,\n        \"created_at\": \"2022-05-23T19:24:28Z\",\n        \"updated_at\": \"2022-05-23T19:24:28Z\"\n    }\n}"
	Value2 = "{\n    \"data\": {\n        \"id\": 2,\n        \"full_name\": \"京都交通票-3日票\",\n        \"short_name\": \"KyotoTransport-ThreeDaysPass\",\n        \"title\": \"京都交通票-3日票\",\n        \"sub_title\": \"3日票\",\n        \"code\": \"6cb8ce2e-a459-48bc-9c19-e562ecae1b27\",\n        \"notice\": \"demo trans for Kansai\",\n        \"release_date\": \"2022-05-23T19:24:28Z\",\n        \"validate_date_s\": \"2022-05-23T19:24:28Z\",\n        \"validate_date_e\": \"2022-05-31T23:59:59Z\",\n        \"status\": 1,\n        \"type\": 2,\n        \"quantity\": -1,\n        \"publisher_id\": 2,\n        \"auto_fetch\": 1,\n        \"created_at\": \"2022-05-23T19:24:28Z\",\n        \"updated_at\": \"2022-05-23T19:24:28Z\"\n    }\n}"
	Value3 = "{\n    \"data\": {\n        \"id\": 3,\n        \"full_name\": \"京都交通票-7日票\",\n        \"short_name\": \"KyotoTransport-SevenDaysPass\",\n        \"title\": \"京都交通票-7日票\",\n        \"sub_title\": \"7日票\",\n        \"code\": \"dd7368b3-aaf2-41f7-90a5-de0762b0f0f5\",\n        \"notice\": \"demo trans for Kansai\",\n        \"release_date\": \"2022-05-23T19:24:28Z\",\n        \"validate_date_s\": \"2022-05-23T19:24:28Z\",\n        \"validate_date_e\": \"2022-05-31T23:59:59Z\",\n        \"status\": 1,\n        \"type\": 2,\n        \"quantity\": -1,\n        \"publisher_id\": 3,\n        \"auto_fetch\": 1,\n        \"created_at\": \"2022-05-23T19:24:28Z\",\n        \"updated_at\": \"2022-05-23T19:24:28Z\"\n    }\n}"
)

var config cachemanager.Config

func SetupRedisConn() {
	/** arrange */
	config = cachemanager.Config{
		Mode:               1,
		Password:           "champ@redis",
		Addr:               []string{"localhost:26379", "localhost:26380", "localhost:26381"},
		MasterName:         "mymaster",
		Db:                 0,
		Wait:               false,
		MaxIdleConnections: 80,
		MaxActive:          0,
		IdleTimeout:        240,
	}
	/** act */
	if err := cachemanager.Mgr.Init(&config); err != nil {
		log.Errorln(err.Error())
	}
}

func TeardownRedisConn() {
	cachemanager.Mgr.Close()
}
