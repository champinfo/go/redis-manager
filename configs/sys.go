package config

import (
	"encoding/json"
	log "github.com/sirupsen/logrus"
	"gitlab.com/champinfo/go/cachemanager"
	"gopkg.in/yaml.v3"
	"io/fs"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"runtime"
	"strings"
)

//SysConfig 宣告SysConfig參數
var SysConfig = &config{}

func init() {
	_, filePath, _, _ := runtime.Caller(0)
	_ = filepath.Walk(path.Dir(filePath), func(path string, info fs.FileInfo, err error) error {
		if !info.IsDir() && strings.HasPrefix(info.Name(), "config") {
			ReadSysConfig(filePath, info.Name())
		}
		return nil
	})
}

type config struct {
	Cache cachemanager.Config `json:"Cache" yaml:"Cache"`
}

func ReadSysConfig(fp, fn string) bool {
	if b, err := ioutil.ReadFile(path.Join(path.Dir(fp), fn)); !os.IsNotExist(err) {
		switch filepath.Ext(fn) {
		case ".yml":
			fallthrough
		case ".yaml":
			err = yaml.Unmarshal(b, &SysConfig)
		case ".json":
			err = json.Unmarshal(b, &SysConfig)
		}
		if err != nil {
			log.Fatalf("read sys config failed, err : %v", err)
		}
		return true
	}
	return false
}
